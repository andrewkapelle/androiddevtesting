base: https://backend.josari.com.au/

Search: https://backend.josari.com.au/api/Search + /(serviceId)

Services: https://backend.josari.com.au/api/Services + /(serviceId) (or) + /(member.serviceId)/MemberAvailability (or) + /\(member.serviceId)/Members?userId=\(member.userId) (or)  + /(serviceId)/Invitation (or) + /(serviceId)/Leave (or) + /(imageId)/Image (or) + /(reviewId)/ReportReview (or) + /(serviceId)/Unblock

Login: https://backend.josari.com.au/api/Account/LocalLogin

Logout: https://backend.josari.com.au/api/Account/Logout

Registration: https://backend.josari.com.au/api/Account/Register

Reset Password: Logout: https://backend.josari.com.au/api/Account/ObtainLocalAccessToken

Images: https://backend.josari.com.au/api/Images

Transaction: https://backend.josari.com.au/api/Transactions + /Outgoing (or) + /Settle (or) + /Incoming/\(serviceId)

Stripe: https://backend.josari.com.au/api/Stripe + https://backend.josari.com.au/api/Stripe/Ephemeral

Categories: https://backend.josari.com.au/api/Categories + /FirstPage

Call: https://backend.josari.com.au/api/Call

Ratings: https://backend.josari.com.au/api/Ratings

Wake-up: https://backend.josari.com.au/WakeUp

Proceed: https://backend.josari.com.au/Proceed

Hangup: https://backend.josari.com.au/Hangup

Respond: https://backend.josari.com.au/Respond

User: https://backend.josari.com.au/User

Provider: https://backend.josari.com.au/Provider
