package io.josari.josariapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class SecondActivity extends AppCompatActivity {

    private static String serverBaseUrl = "https://backend.josari.com.au/";
    private static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //Set terms of service
        TextView termsView = findViewById(R.id.termsOfService);

        String termsText = "By creating an account, I accept Josari's Terms of Service";

        SpannableString ss = new SpannableString(termsText);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(SecondActivity.this, "One", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(Color.parseColor("#777777"));
                ds.setUnderlineText(true);
            }
        };

        ss.setSpan(clickableSpan1, 42, 58, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termsView.setText(ss);
        termsView.setMovementMethod(LinkMovementMethod.getInstance());

        setHomeBar();

        //url
        final String url = serverBaseUrl + "api/Search";

        //Get access token from shared preference
        SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final String accessToken = sharedpreferences.getString("access_token", null);

        //implement search function
        final EditText search = findViewById(R.id.searchText);
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if(actionId == EditorInfo.IME_ACTION_DONE
                        || keyEvent.getAction() == KeyEvent.ACTION_DOWN
                        || keyEvent.getAction() == KeyEvent.KEYCODE_ENTER) {
                    String searchTerm = search.getText().toString();
                    //Async search
                    CallBackendSync callBackendSync = new CallBackendSync();
                    String [] params = {searchTerm, url, accessToken};
                    callBackendSync.execute(params);
                    return true;

                }
                return false;
            }
        });
    }

    private void setHomeBar() {
        final Button signup = findViewById(R.id.login_btn);
        signup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(SecondActivity.this, io.josari.josariapp.signup.class));
            }
        });

        final Button home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(SecondActivity.this, SecondActivity.class));
            }
        });

        final Button favourites = findViewById(R.id.star_button);
        favourites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(SecondActivity.this, Favourite_Page.class));
            }
        });

        final Button profile_page = findViewById(R.id.person_button);
        profile_page.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        final Button notifications = findViewById(R.id.notification_button);
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(SecondActivity.this, Notification_Page.class));
            }
        });

        final Button service = findViewById(R.id.service_button);
        service.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(SecondActivity.this, services.class));
            }
        });

        final TextView advanced_search = findViewById(R.id.rating);
        advanced_search.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                startActivity(new Intent(SecondActivity.this, io.josari.josariapp.advanced_search.class));
            }
        });
    }

    class CallBackendSync extends AsyncTask {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected Object doInBackground(Object [] objects) {
            String searchTerm = (String) objects[0];
            String url = (String) objects[1];
            String token = (String) objects[2];

            //Search Body
            RequestBody searchBody = new FormBody.Builder()
                    .add("QueryText", searchTerm)
                    .add("ChargeOutPerMinuteFrom", "0")
                    .add("ChargeOutPerMinuteTo", "500")
                    .add("longitude", "0")
                    .add("latitude", "0")
                    .add("page", "0")
                    .build();
            //Create request
            Request.Builder builder = new Request.Builder();
            builder.url(url);
            builder.addHeader("Accept", "application/json");
            if (token != null) {
                builder.addHeader("Authorization", token);
            }
            builder.post(searchBody);
            Request request = builder.build();
            try {
                Response response = client.newCall(request).execute();
                String results = response.body().string();
                Intent searchIntent = new Intent(SecondActivity.this, search_page.class);
                //Create and add Bundle of extra
                Bundle resultSet = new Bundle();
                resultSet.putString("Search", searchTerm);
                resultSet.putString("Results", results);
                searchIntent.putExtras(resultSet);
                startActivity(searchIntent);
                return results;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s){
            super.onPostExecute(s);
        }
    }

}