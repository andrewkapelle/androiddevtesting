package io.josari.josariapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class edit_profile extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Is logged in?
        final isLoggedIn loggedIn = new isLoggedIn(getApplicationContext());
        String accessToken = loggedIn.getToken();

        setContentView(R.layout.activity_edit_profile);


        String url = apiCallPoints.userInfo;
        TextView us1 = findViewById(R.id.editFullName);
        TextView us2 = findViewById(R.id.editEmail);
        ImageButton profile_pic = findViewById((R.id.editpicbutton));

        //Call and set fullname and profile
        final OkHttpClient client = new OkHttpClient();

        //Create request
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", "Bearer " + accessToken)
                .build();

        //Make Call
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast errorMessage = Toast.makeText(getApplicationContext(), "Communication Failed", Toast.LENGTH_LONG);
                errorMessage.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    String fullName = Jobject.getString("Fullname");
                    String email = Jobject.getString("Email");
                    String picture = Jobject.getString("Picture");
                    loggedIn.setProfile(fullName, email, picture);
                } catch(JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        String fullName = loggedIn.getFullName();
        String email = loggedIn.getEmail();
        String picture = loggedIn.getPicture();


        us1.setText(fullName);
        us2.setText(email);
        profile_pic.setScaleType(ImageView.ScaleType.FIT_END);
        Picasso.get().load(picture).into(profile_pic);

        final Button home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(edit_profile.this, SecondActivity.class));
            }
        });

        final Button favourites = findViewById(R.id.star_button);
        favourites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(edit_profile.this, Favourite_Page.class));
            }
        });

        final Button profile_page = findViewById(R.id.person_button);
        profile_page.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        final Button notifications = findViewById(R.id.notification_button);
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(edit_profile.this, Notification_Page.class));
            }
        });

        final Button service = findViewById(R.id.service_button);
        service.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(edit_profile.this, services.class));
            }
        });

    }

    public void profile_btn(View view){
        startActivity(new Intent(getApplicationContext(), edit_profile_pic.class));
    }

    public void back_btn(View view){
        startActivity(new Intent(getApplicationContext(), Profile.class));
    }

    public void save_btn(View view){
        String url = apiCallPoints.userInfoUpdate;
        //Get Email
        final TextView emailField = findViewById(R.id.editEmail);
        final String emailString = emailField.getText().toString();

        //Get Full Name
        final TextView fullnameField = findViewById(R.id.editFullName);
        final String fullnameString = fullnameField.getText().toString();

        //Put Email, Password, and Full Name in shared preference
        isLoggedIn loggedIn = new isLoggedIn(getApplicationContext());
        String accessToken = loggedIn.getToken();

        //final SharedPreferences.Editor editor = sharedpreferences.edit();

        String imageName = loggedIn.getPicture();
        final String image = imageName;

        //Check
        Log.i("SignUp FullName", fullnameString);
        Log.i("SignUp Email", emailString);

        //Setup OKhttp3
        final OkHttpClient client = new OkHttpClient();

        JSONObject jsonbody = new JSONObject();
        try{
            jsonbody.put("Fullname", fullnameString);
            jsonbody.put("Email", emailString);
            jsonbody.put("Picture", image);
        } catch( JSONException e){
            e.printStackTrace();
        }

        String jsonString = jsonbody.toString();
        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonString);

        //Create request
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", "Bearer " + accessToken)
                .post(body)
                .build();

        //Make Call
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast errorMessage = Toast.makeText(getApplicationContext(), "Communication Failed", Toast.LENGTH_LONG);
                errorMessage.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                loggedIn.setProfile(fullnameString, image, emailString);
                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        startActivity(new Intent(getApplicationContext(), Profile.class));
    }


}
