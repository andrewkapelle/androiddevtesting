package io.josari.josariapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.sinch.android.rtc.Sinch;
import com.sinch.android.rtc.SinchClient;
import com.squareup.picasso.Picasso;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class precall extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_precall);

        //Set Search Text
        final Bundle memberDetails = getIntent().getBundleExtra("Member");

        //Populate screen
        ImageView avatar = findViewById(R.id.avatar);
        Picasso.get().load(memberDetails.getString("Logo")).into(avatar);
        TextView memberName = findViewById(R.id.membername);
        memberName.setText(memberDetails.getString("Name"));
        TextView rating = findViewById(R.id.rating);
        rating.setText(memberDetails.getString("Rating") + " ★");
        TextView serviceName = findViewById(R.id.servicename);
        serviceName.setText(memberDetails.getString("Service"));
        TextView overview = findViewById(R.id.overview);
        overview.setText(memberDetails.getString("Overview"));

        //Add button clicks
        Button call = findViewById(R.id.yes_button);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Button Pressed", "Understood");
                /*CallBackendSync callBackendSync = new CallBackendSync();
                String [] params = {"https://backend.josari.com.au/api/Call"};
                callBackendSync.execute(params);
                SinchClient sinchClient = Sinch.getSinchClientBuilder()
                        .context(getApplicationContext())
                        .userId("current-user-id")
                        .applicationKey("b929902f-2796-4e8c-b278-18ccb73ade48")
                        .applicationSecret("x8CoNA41qUiD3+yMZLEPNg==")
                        .environmentHost("clientapi.sinch.com")
                        .build();
                sinchClient.setSupportCalling(true);
                sinchClient.startListeningOnActiveConnection();
                sinchClient.start();*/
                Intent callIntent = new Intent(precall.this, call_engaged.class);
                startActivity(callIntent);
            }
        });
        Button cancel = findViewById(R.id.cancel_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        setHomeBar();
    }

    private void setHomeBar() {
        final Button home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(precall.this, SecondActivity.class));
            }
        });

        final Button favourites = findViewById(R.id.star_button);
        favourites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(precall.this, Favourite_Page.class));
            }
        });

        final Button profile_page = findViewById(R.id.person_button);
        profile_page.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        final Button notifications = findViewById(R.id.notification_button);
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(precall.this, Notification_Page.class));
            }
        });

        final Button service = findViewById(R.id.service_button);
        service.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(precall.this, services.class));
            }
        });

        final Button call = findViewById(R.id.yes_button);
        call.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(precall.this, call_pre_engaged.class));
            }
        });
    }

    class CallBackendSync extends AsyncTask {

        OkHttpClient client = new OkHttpClient();

        @Override
        protected Object doInBackground(Object [] objects) {
            String url = (String) objects[0];

            //Search Body
            RequestBody searchBody = new FormBody.Builder()
                    .add("longitude", "0")
                    .add("latitude", "0")
                    .add("page", "0")
                    .build();
            //Create request
            Request.Builder builder = new Request.Builder();
            builder.url(url);
            builder.addHeader("Accept", "application/json");
            builder.post(searchBody);
            Request request = builder.build();
            try {
                Response response = client.newCall(request).execute();
                Log.i("Results", response.body().string());
                Intent callIntent = new Intent(precall.this, call_engaged.class);
                startActivity(callIntent);
                return null;
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String s){
            super.onPostExecute(s);
        }
    }
}
