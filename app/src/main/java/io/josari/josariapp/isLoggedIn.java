package io.josari.josariapp;

import android.content.Context;
import android.content.SharedPreferences;

public class isLoggedIn {

    public static final String MyPREFERENCES = "LoggedInUser";
    private SharedPreferences sharedpreferences;

    public isLoggedIn(Context context) {
        sharedpreferences = context.getSharedPreferences(isLoggedIn.MyPREFERENCES, Context.MODE_PRIVATE);
    }

    public String getFullName() {
        return sharedpreferences.getString("fullName", null);
    }

    public String getPicture() {
        return sharedpreferences.getString("imageName", null);
    }
    public String getEmail() {
        return sharedpreferences.getString("email", null);
    }

    public void setPicture(String imageName){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("imageName", imageName);
        editor.apply();
    }

    public void setProfile(String fullName, String picture, String email) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("fullName", fullName);
        editor.putString("imageName", picture);
        editor.putString("email", email);
        editor.apply();
    }

    public void storeLoginDetails (String emailString, String passwordString){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Username", emailString);
        editor.putString("Password", passwordString);
        editor.apply();
    }

    public void storeToken (String token){
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Token", token);
        editor.apply();
    }

    public String getToken (){
        return sharedpreferences.getString("Token", null);
    }

    public SharedPreferences getPref () {
        return this.sharedpreferences;
    }

    public void logout() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.apply();
    }
}
