package io.josari.josariapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class search_page extends AppCompatActivity {

    private String imageURL = "https://backend.josari.com.au/Uploads/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search_page);

        //Set Search Text
        Bundle resultSet = getIntent().getExtras();
        String searchTerm = resultSet.getString("Search");
        EditText searchText = findViewById(R.id.searchText);
        searchText.setText(searchTerm);

        //Get Results
        String results = resultSet.getString("Results");

        //Display Search
        try {
            displayResults(new JSONObject(results));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        setHomeBar();
    }

    private void displayResults(JSONObject resultsObject) throws JSONException{
        LinearLayout resultsLinear = findViewById(R.id.resultsPlane);
        LayoutInflater li =  (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try {
            JSONArray resultsArray = resultsObject.getJSONArray("Results");
            for(int count = 0; count < resultsArray.length(); count++){
                //Constraint Layout to contain all results
                ConstraintLayout resultView = (ConstraintLayout) li.inflate(R.layout.template_result_card, null);
                resultView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                JSONObject result = resultsArray.getJSONObject(count);
                //Set ID
                resultView.setId(count);

                //Get Service Details
                final String serviceName = result.getString("Name");
                final String rating = result.getString("Rating");
                final String logo = imageURL + result.getString("Logo");

                //Get Member Details
                JSONArray memberArray = result.getJSONArray("Members");
                JSONObject member = (JSONObject)memberArray.get(0);
                final int JosariId =  member.getInt("JosariId");
                final String fullName =  member.getString("Fullname");
                final String chargePerMinuteRate = member.getString("ChargePerMinuteRate");
                final String overview = member.getString("Overview");
                final String address = member.getString("Address");

                //Set Image Button
                ImageButton portrait = (ImageButton) resultView.getViewById(R.id.avatar);
                Picasso.get().load(logo).into(portrait);
                portrait.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent makeCall = new Intent(search_page.this, precall.class);
                        Bundle memberDetails = new Bundle();
                        memberDetails.putInt("JosariId", JosariId);
                        memberDetails.putString("Name", fullName);
                        memberDetails.putString("Service", serviceName);
                        memberDetails.putString("Rating", rating);
                        memberDetails.putString("Logo", logo);
                        memberDetails.putString("Price", chargePerMinuteRate);
                        memberDetails.putString("Overview", overview);
                        memberDetails.putString("Address", address);
                        makeCall.putExtra("Member", memberDetails);
                        startActivity(makeCall);
                    }
                });

                //Set Price
                TextView price = (TextView) resultView.getViewById(R.id.price);
                price.setText(chargePerMinuteRate);

                //Set Member Name
                TextView memberName = (TextView) resultView.getViewById(R.id.memberName);
                memberName.setId(JosariId);
                memberName.setText(fullName);

                //Set Service Name
                TextView service = (TextView) resultView.getViewById(R.id.serviceName);
                service.setText(serviceName);

                //Set Blurb
                TextView blurb = (TextView) resultView.getViewById(R.id.overview);
                blurb.setText(overview);

                //Set Rating + Distance
                TextView finish = (TextView) resultView.getViewById(R.id.locationRating);
                finish.setText(rating + " ★   | " + address);

                //Add Layout to Linear Layout
                resultsLinear.addView(resultView);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setHomeBar() {
        //Draw Home Bar
        final Button home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                startActivity(new Intent(search_page.this, SecondActivity.class));
            }
        });

        final Button favourites = findViewById(R.id.star_button);
        favourites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                startActivity(new Intent(search_page.this, Favourite_Page.class));
            }
        });

        final Button profile_page = findViewById(R.id.person_button);
        profile_page.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        final Button notifications = findViewById(R.id.notification_button);
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                startActivity(new Intent(search_page.this, Notification_Page.class));
            }
        });

        final Button service = findViewById(R.id.service_button);
        service.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                startActivity(new Intent(search_page.this, services.class));
            }
        });
    }
}
