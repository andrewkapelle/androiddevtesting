package io.josari.josariapp;

public class apiCallPoints {
    public static String serverBaseUrl = "https://backend.josari.com.au/";
    public static String account = serverBaseUrl + "api/Account";
    public static String search = serverBaseUrl + "api/Search";
    public static String notification = serverBaseUrl + "api/Notifications";
    public static String services = serverBaseUrl + "api/Services";
    public static String stripe = serverBaseUrl + "api/Stripe";
    public static String registration = account + "/Register";
    public static String changePassword = account + "/ChangePassword";
    public static String requestPasswordReset = account + "/PassCode";
    public static String resetPassword = account + "/ResetPassword";
    public static String login = account + "/LocalLogin";
    public static String logout = account + "/Logout";
    public static String userInfo = account + "/UserInfo";
    public static String userInfoUpdate = account + "/Update";
    public static String owesAmount = account + "/Owes";
    public static String block = account + "/Blocked";
    public static String stripeInfo = stripe + "/Info";
    public static String imageURL = serverBaseUrl+ "Uploads/";
    public static String transaction = serverBaseUrl+ "api/Transactions/Outgoing";


}
