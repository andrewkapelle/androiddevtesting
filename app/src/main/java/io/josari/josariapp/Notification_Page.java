package io.josari.josariapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Notification_Page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        //Is logged in?
        final isLoggedIn loggedIn = new isLoggedIn(getApplicationContext());
        String accessToken = loggedIn.getToken();
        //Log.i("Token", accessToken);

        if(accessToken == null) {
            startActivity(new Intent(getApplicationContext(), Login_page.class));
        }

        if(accessToken != null){
            setContentView(R.layout.activity_notification__page);

            final OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(apiCallPoints.notification)
                    .addHeader("Authorization", "Bearer " + accessToken)
                    .addHeader("Accept", "application/json")
                    .build();

            //Make Call
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Toast errorMessage = Toast.makeText(getApplicationContext(), "Communication Failed", Toast.LENGTH_LONG);
                    errorMessage.show();
                    call.cancel();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    //Log.i("Response", response.body().string());
                    String results = response.body().string();
                    try {
                        if (!results.isEmpty()) {
                            displayResults(new JSONArray(results));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        call.cancel();
                    }
                }
            });

        }

        final Button home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(Notification_Page.this, SecondActivity.class));
            }
        });

        final Button favourites = findViewById(R.id.star_button);
        favourites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(Notification_Page.this, Favourite_Page.class));
            }
        });

        final Button profile_page = findViewById(R.id.person_button);
        profile_page.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        final Button notifications = findViewById(R.id.notification_button);
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(Notification_Page.this, Notification_Page.class));
            }
        });

        final Button service = findViewById(R.id.service_button);
        service.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(Notification_Page.this, services.class));
            }
        });

    }

    private void displayResults(JSONArray resultsArray) throws JSONException{

        LinearLayout resultsLinear = findViewById(R.id.resultsPlane);
        LayoutInflater li =  (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        try{
            for(int count = 0; count < resultsArray.length(); count++){
                ConstraintLayout resultView = (ConstraintLayout) li.inflate(R.layout.notification_result_card, null);
                resultView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                JSONObject result = resultsArray.getJSONObject(count);

                //Set ID
                resultView.setId(count);
                final CardView notif = (CardView) resultView.getViewById(R.id.notif_card);

                final TextView title = (TextView) notif.getChildAt(0);
                final TextView desc = (TextView) notif.getChildAt(1);

                final String notif_title = result.getString("Title");
                final String notif_desc = result.getString("Message");


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        title.setText(notif_title);
                        desc.setText(notif_desc);

                        //Add Layout to Linear Layout
                        resultsLinear.addView(resultView);
                    }
                });

            }

        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
