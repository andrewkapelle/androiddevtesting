package io.josari.josariapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class edit_profile_pic extends AppCompatActivity {

    Uri imageUri;
    Uri resultUri;
    File file;


    public static final String MyPREFERENCES = "MyPrefs";

    private CircleImageView ProfileImage;
    private static final int PICK_IMAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profilepicture);

        //Is logged in?
        final isLoggedIn loggedIn = new isLoggedIn(getApplicationContext());
        String accessToken = loggedIn.getToken();

        //Get current profile pic
        String picture = loggedIn.getPicture();
        String imageURL = apiCallPoints.imageURL + picture;

        ProfileImage = (CircleImageView) findViewById(R.id.Profile_Image);
        Picasso.get().load(imageURL).into(ProfileImage);


        ProfileImage.setOnClickListener(v -> {

            Intent gallery = new Intent();
            gallery.setType("image/*");
            gallery.setAction(Intent.ACTION_GET_CONTENT);

            startActivityForResult(Intent.createChooser(gallery, "Select Picture"), PICK_IMAGE);


        });



    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null) {
            imageUri = data.getData();

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), resultUri);
                    ProfileImage.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

    }


    public void back_btn(View view){
        startActivity(new Intent(getApplicationContext(), edit_profile.class));
    }

    public void save_btn(View view){
        final isLoggedIn loggedIn = new isLoggedIn(getApplicationContext());
        String accessToken = loggedIn.getToken();

        String filePath = resultUri.getPath();
        file = new File(filePath);
        String filename = filePath.substring(filePath.lastIndexOf("/") + 1);
        MediaType MEDIA_TYPE_IMG = MediaType.parse("image/*");

        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("image", filename, RequestBody.create(MEDIA_TYPE_IMG, file))
                .build();

        Request request = new Request.Builder()
                .url(apiCallPoints.imageURL)
                .header("Authorization", "Bearer " + accessToken)
                .post(requestBody)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast errorMessage = Toast.makeText(getApplicationContext(), "Communication Failed", Toast.LENGTH_LONG);
                errorMessage.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String imageName = response.body().string();
                imageName = imageName.replace("\"", "");
                Log.w("tag", imageName);
                loggedIn.setPicture(imageName);
                startActivity(new Intent(getApplicationContext(), edit_profile.class));
            }
        });
    }
}
