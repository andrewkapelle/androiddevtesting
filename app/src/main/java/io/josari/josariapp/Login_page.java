package io.josari.josariapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login_page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login__page);

        final Button home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(Login_page.this, SecondActivity.class));
            }
        });

        final Button favourites = findViewById(R.id.star_button);
        favourites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(Login_page.this, Favourite_Page.class));
            }
        });

        final Button profile_page = findViewById(R.id.person_button);
        profile_page.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        final Button notifications = findViewById(R.id.notification_button);
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(Login_page.this, Notification_Page.class));
            }
        });

        final Button service = findViewById(R.id.service_button);
        service.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(Login_page.this, services.class));
            }
        });

        final Button signup = findViewById(R.id.signup_link);
        signup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                startActivity(new Intent(getApplicationContext(), signup.class));
            }
        });
    }

    /*
     * Login in user to backend
     */
    public void login (View v) {
        //url
        String url = apiCallPoints.login;
        //Get Email
        TextView emailField = findViewById(R.id.input_email);
        String emailString = emailField.getText().toString();
        emailString = emailString.trim();
        //Get Password
        TextView passwrodField = findViewById(R.id.input_password);
        String passwrodString = passwrodField.getText().toString();
        passwrodString = passwrodString.trim();

        //Setup OKhttp3
        final OkHttpClient client = new OkHttpClient();
        //Authentication Body
        RequestBody authBody = new FormBody.Builder()
                .add("Username", emailString)
                .add("Password", passwrodString)
                .add("GrantType", "password")
                .build();
        //Create request
        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .post(authBody)
                .build();

        //Make Call
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast errorMessage = Toast.makeText(getApplicationContext(), "Communication Failed", Toast.LENGTH_LONG);
                errorMessage.show();
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    System.out.println("Results: " + Jobject.toString());
                    String accessToken = Jobject.getString("access_token");
                    isLoggedIn loggedIn = new isLoggedIn(getApplicationContext());
                    loggedIn.storeToken(accessToken);
                    startActivity(new Intent(getApplicationContext(), Profile.class));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
