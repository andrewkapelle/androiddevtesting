package io.josari.josariapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;

import androidx.appcompat.app.AppCompatActivity;

public class callOverview extends AppCompatActivity {

    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_overview);

        ratingBar = findViewById(R.id.ratingBar);
        ratingBar.setNumStars(5);

        final Button rating = findViewById(R.id.rating_btn);
        rating.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(callOverview.this, Rating_Evaluation_Page.class));
            }
        });
    }
}
