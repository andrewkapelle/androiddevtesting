package io.josari.josariapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class signup extends AppCompatActivity {

    private static String serverBaseUrl = "https://backend.josari.com.au/";
    private static final String MyPREFERENCES = "MyPrefs" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        final Button home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(signup.this, SecondActivity.class));
            }
        });

        final Button favourites = findViewById(R.id.star_button);
        favourites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(signup.this, Favourite_Page.class));
            }
        });

        final Button profile_page = findViewById(R.id.person_button);
        profile_page.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        final Button notifications = findViewById(R.id.notification_button);
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(signup.this, Notification_Page.class));
            }
        });

        final Button service = findViewById(R.id.service_button);
        service.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(signup.this, services.class));
            }
        });


        final Button login = findViewById(R.id.login_link);
        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(signup.this, Login_page.class));
            }
        });
    }

    /*
     * Register in user to backend
     */
    public void register (View v) {
        String url = serverBaseUrl + "api/Account/Register";
        //Get Email
        final TextView emailField = findViewById(R.id.input_email);
        final String emailString = emailField.getText().toString();
        //Get Password
        final TextView passwrodField = findViewById(R.id.input_password);
        final String passwrodString = passwrodField.getText().toString();
        //Get Full Name
        final TextView fullnameField = findViewById(R.id.input_fullname);
        final String fullnameString = fullnameField.getText().toString();

        final TextView errorField = findViewById(R.id.error_msg);
        final String error = "";

        //Put Email, Password, and Full Name in shared preference
        SharedPreferences sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("Fullname", fullnameString);
        editor.putString("Username", emailString);
        editor.putString("Password", passwrodString);
        editor.apply();

        //Check
        Log.i("SignUp FullName", fullnameString);
        Log.i("SignUp Email", emailString);
        Log.i("SignUp Password", passwrodString);

        //Setup OKhttp3
        final OkHttpClient client = new OkHttpClient();
        //Authentication Body
        RequestBody registrationBody = new FormBody.Builder()
                .add("email", emailString)
                .add("fullname", fullnameString)
                .add("password", passwrodString)
                .build();

        //Create request
        Request request = new Request.Builder()
                .url(url)
                .post(registrationBody)
                .build();

        //Make Call
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast errorMessage = Toast.makeText(getApplicationContext(), "Communication Failed", Toast.LENGTH_LONG);
                errorMessage.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                //Log.i("Response", response.body().string());
                String jsonData = response.body().string();
                try {
                    JSONObject Jobject = new JSONObject(jsonData);
                    if (!Jobject.isNull("ModelState")){
                        if(Jobject.getString("ModelState").contains("taken")){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    errorField.setText("Username already exist. Try logging in instead?");
                                }
                            });
                        }else if (Jobject.getString("ModelState").contains("Password")){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    errorField.setText("Please enter more than 7 characters  and at least 1 digit for password.");
                                }
                            });
                        } else if (Jobject.getString("ModelState").contains("Fullname")){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    errorField.setText("Please fill the fullname field.");
                                }
                            });
                        }
                    }
                    String accessToken = Jobject.getString("access_token");
                    editor.putString("accessKey", accessToken);
                    editor.apply();
                    startActivity(new Intent(getApplicationContext(), Profile.class));
                } catch (JSONException e) {
                    e.printStackTrace();
                    call.cancel();
                }
            }
        });
    }
}
