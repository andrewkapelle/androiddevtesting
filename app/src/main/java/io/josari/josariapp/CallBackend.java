package io.josari.josariapp;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CallBackend {

    private static String serverBaseUrl = "https://backend.josari.com.au/";
    private String url = "";
    private String token = null;
    //JSON Object
    private JSONObject Jobject = null;

    public CallBackend(String url, String token){
        this.url = serverBaseUrl + url;
        this.token = token;
        this.Jobject = null;
    }

    public CallBackend(String url){
        this.url = serverBaseUrl + url;
        this.Jobject = null;
    }

    public void performSearch(String searchTerm) {

        //Setup OKhttp3
        final OkHttpClient client = new OkHttpClient();

        //Search Body
        RequestBody searchBody = new FormBody.Builder()
                .add("QueryText", searchTerm)
                .add("Categories", "")
                .add("ChargeOutPerMinuteFrom", "0")
                .add("ChargeOutPerMinuteTo", "0")
                .add("maxDistance", "0")
                .add("longitude", "0")
                .add("latitude", "0")
                .add("page", "0")
                .build();
        //Create request
        Request request = null;

        if (token != null) {
            request = new Request.Builder()
                    .url(url)
                    .addHeader("Authorization", token)
                    .addHeader("Accept", "application/json")
                    .post(searchBody)
                    .build();
        } else {
            request = new Request.Builder()
                    .url(url)
                    .addHeader("Accept", "application/json")
                    .post(searchBody)
                    .build();
        }

        //Make Call
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String jsonData = response.body().string();
                setResults(jsonData);
            }
        });
    }

    public void setResults(String jString) {
        if(this.Jobject == null){
            try {
                this.Jobject = new JSONObject(jString);
                Log.i("Raw JSON", jString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("JSON Object Not Null", jString);
        }
    }

    public JSONObject getResults(){
        return this.Jobject;
    }

}
