package io.josari.josariapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Profile extends AppCompatActivity {

    String profilePref = "My Profile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //Is logged in?
        final isLoggedIn loggedIn = new isLoggedIn(getApplicationContext());
        String accessToken = loggedIn.getToken();

        if(accessToken == null) {
            startActivity(new Intent(getApplicationContext(), Login_page.class));
        }

        if(accessToken != null){

            setContentView(R.layout.activity_profile);

            String url = apiCallPoints.userInfo;
            TextView us1 = findViewById(R.id.profileFullName);
            ImageView profile_pic = findViewById((R.id.imageView));

            TextView us2 = findViewById(R.id.profileRating);

            //Call and set fullname and profile
            final OkHttpClient client = new OkHttpClient();

            //Create request
            Request request = new Request.Builder()
                    .url(url)
                    .header("Authorization", "Bearer " + accessToken)
                    .build();

            //Make Call
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Toast errorMessage = Toast.makeText(getApplicationContext(), "Communication Failed", Toast.LENGTH_LONG);
                    errorMessage.show();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String jsonData = response.body().string();
                    try {
                        JSONObject Jobject = new JSONObject(jsonData);
                        String fullName = Jobject.getString("Fullname");
                        String picture = Jobject.getString("Picture");
                        String email = Jobject.getString("Email");
                        loggedIn.setProfile(fullName, picture, email);


                    } catch(JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


            ad();

            //Setup Profile
            String fullName = loggedIn.getFullName();
            String picture = loggedIn.getPicture();

            System.out.println("Test" + fullName);

            us1.setText(fullName);
            String imageURL = apiCallPoints.imageURL + picture;
            profile_pic.setScaleType(ImageView.ScaleType.FIT_END);
            Picasso.get().load(imageURL).into(profile_pic);

            final Button home = findViewById(R.id.home_button);
            home.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    startActivity(new Intent(Profile.this, SecondActivity.class));
                }
            });

            final Button favourites = findViewById(R.id.star_button);
            favourites.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    startActivity(new Intent(Profile.this, Favourite_Page.class));
                }
            });

            final Button profile_page = findViewById(R.id.person_button);
            profile_page.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    startActivity(new Intent(Profile.this, Profile.class));
                }
            });

            final Button notifications = findViewById(R.id.notification_button);
            notifications.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    startActivity(new Intent(Profile.this, Notification_Page.class));
                }
            });

            final Button service = findViewById(R.id.service_button);
            service.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    startActivity(new Intent(Profile.this, services.class));
                }
            });

            final Button edit_profile = findViewById(R.id.edit_service_btn);
            edit_profile.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    startActivity(new Intent(Profile.this, io.josari.josariapp.edit_profile.class));
                }
            });

            final Button transaction_history = findViewById(R.id.transaction_history_btn);
            transaction_history.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    startActivity(new Intent(Profile.this, Transaction_History_Profile.class));
                }
            });

            final Button blocked_accounts = findViewById(R.id.blocked_accounts_btn);
            blocked_accounts.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    // Code here executes on main thread after user presses button

                    startActivity(new Intent(Profile.this, Blocked_Account_Profile.class));
                }
            });
        }
    }

    public void ad(){
        System.out.println("Add");
    }

    /*
     * Logout User
     */
    public void logout (View v) {
        String url = apiCallPoints.logout;

        //Put Email, Password, and Full Name in shared preference
        isLoggedIn loggedIn = new isLoggedIn(getApplicationContext());
        String accessToken = loggedIn.getToken();

        //Setup OKhttp3
        final OkHttpClient client = new OkHttpClient();

        //Create request
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", "Bearer " + accessToken)
                .build();

        //Make Call
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Toast errorMessage = Toast.makeText(getApplicationContext(), "Communication Failed", Toast.LENGTH_LONG);
                errorMessage.show();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                loggedIn.logout();
                startActivity(new Intent(getApplicationContext(), Login_page.class));
            }
        });
    }
}
