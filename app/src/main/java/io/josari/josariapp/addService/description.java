package io.josari.josariapp.addService;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import io.josari.josariapp.Favourite_Page;
import io.josari.josariapp.Notification_Page;
import io.josari.josariapp.Profile;
import io.josari.josariapp.R;
import io.josari.josariapp.SecondActivity;
import io.josari.josariapp.services;

public class description extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        final EditText et = (EditText)findViewById(R.id.input_shortDescription);
        et.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                TextView tv = (TextView)findViewById(R.id.length_counter);
                tv.setText(String.valueOf(0 + et.length() + " / 200"));
            }

            @Override
            public void onTextChanged(CharSequence s, int st, int b, int c)
            { }
            @Override
            public void beforeTextChanged(CharSequence s, int st, int c, int a)
            { }
        });

        final Button home = findViewById(R.id.home_button);
        home.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(description.this, SecondActivity.class));
            }
        });

        final Button favourites = findViewById(R.id.star_button);
        favourites.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(description.this, Favourite_Page.class));
            }
        });

        final Button profile_page = findViewById(R.id.person_button);
        profile_page.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(getApplicationContext(), Profile.class));
            }
        });

        final Button notifications = findViewById(R.id.notification_button);
        notifications.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(description.this, Notification_Page.class));
            }
        });

        final Button service = findViewById(R.id.service_button);
        service.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(description.this, services.class));
            }
        });

        final Button next = findViewById(R.id.nextBtn);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(description.this, experience.class));
            }
        });

        final Button back = findViewById(R.id.backBtn);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(description.this, rates.class));
            }
        });
    }
}
