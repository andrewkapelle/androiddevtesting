package io.josari.josariapp;

import android.content.Context;
import android.widget.ImageButton;

import androidx.constraintlayout.widget.ConstraintLayout;

public class ResultView {

    private ConstraintLayout resultCard = null;
    private Context context = null;

    public ResultView (Context currentContext) {
        this.context = currentContext;
        resultCard = new ConstraintLayout(currentContext);

    }



    private ImageButton setPortait(String uri){

        return  new ImageButton(context);
    }


    public ConstraintLayout create(){
        return this.resultCard;
    }

    //Getters and Setters
    public ConstraintLayout getResultCard() {
        return this.resultCard;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context currentContext) {
        this.context = currentContext;
    }
}
