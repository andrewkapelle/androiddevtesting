package io.josari.josariapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class call_pre_engaged extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_pre_engaged);

        final Button engagedButton = findViewById(R.id.engageButton);
        engagedButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(call_pre_engaged.this, call_engaged.class));
            }
        });

        final Button endCall = findViewById(R.id.endcall);
        endCall.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button

                startActivity(new Intent(call_pre_engaged.this, callOverview.class));
            }
        });
    }
}
